## Goals ##

* Create a ready-to-release flow.
* Understand how "Jetpack compose" integrates with the rest of the Android ecosystem.

### What is this repository for? ###

#### Functional ####
* Display a list of transactions.
* When a transaction is clicked, display a detail view of it.
* Handle different states for each screen (error, loading, etc).

#### Non Functional ####
* Implement the solution using a Clean Architecture.
* Integrate with a DI tool (Hilt).
* Handle navigation between screen using a compose approach (do not use fragments).
* Add test cases for the view.
* Accessibility support.
