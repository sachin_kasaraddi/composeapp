package com.hackathon.composersapp.view.detail

import androidx.compose.ui.test.*
import androidx.compose.ui.test.junit4.createComposeRule
import com.hackathon.composersapp.model.TransactionType.Bill
import com.hackathon.composersapp.model.TransactionType.Food
import com.hackathon.composersapp.model.TransactionType.Health
import com.hackathon.composersapp.model.TransactionType.Grocery
import com.hackathon.composersapp.view.theme.ComposersAppTheme
import org.junit.Rule
import org.junit.Test

class TransactionTypeViewTest {

    @get:Rule
    val composeTestRule = createComposeRule()

    @Test
    fun givenTransactionTypeGrocery_itShouldRenderIt() {
        composeTestRule.setContent { ComposersAppTheme { TransactionTypeView(type = Grocery) } }

        composeTestRule.onNodeWithText(text = "Grocery").assertIsDisplayed()
    }

    @Test
    fun givenTransactionTypeBill_itShouldRenderIt() {
        composeTestRule.setContent { ComposersAppTheme { TransactionTypeView(type = Bill) } }

        composeTestRule.onNodeWithText(text = "Bill").assertIsDisplayed()
    }

    @Test
    fun givenTransactionTypeHealth_itShouldRenderIt() {
        composeTestRule.setContent { ComposersAppTheme { TransactionTypeView(type = Health) } }

        composeTestRule.onNodeWithText(text = "Health").assertIsDisplayed()
    }

    @Test
    fun givenTransactionTypeFood_itShouldRenderIt() {
        composeTestRule.setContent { ComposersAppTheme { TransactionTypeView(type = Food) } }

        composeTestRule.onNodeWithText(text = "Food").assertIsDisplayed()
    }
}