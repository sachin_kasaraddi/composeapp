package com.hackathon.composersapp.view.detail

import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithText
import com.hackathon.composersapp.view.theme.ComposersAppTheme
import org.junit.Rule
import org.junit.Test

class ErrorTransactionDetailViewTest {

    @get:Rule
    val composeTestRule = createComposeRule()

    @Test
    fun givenTransactionTypeGrocery_itShouldRenderIt() {
        composeTestRule.setContent { ComposersAppTheme { ErrorTransactionDetailView() } }

        composeTestRule.onNodeWithText(text = "Ups! Something went wrong.").assertIsDisplayed()
        composeTestRule.onNodeWithText(text = "Our team is working to fix it the sooner we can. Please, try in a couple of minutes.").assertIsDisplayed()
    }
}