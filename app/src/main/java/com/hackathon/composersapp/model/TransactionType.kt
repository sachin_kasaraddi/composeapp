package com.hackathon.composersapp.model

sealed class TransactionType {
    object Grocery : TransactionType()
    object Bill : TransactionType()
    object Health : TransactionType()
    object Food : TransactionType()
}