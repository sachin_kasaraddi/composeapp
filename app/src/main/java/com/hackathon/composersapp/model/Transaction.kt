package com.hackathon.composersapp.model

import java.math.BigDecimal
import java.util.*

data class Transaction(
    val id: String = UUID.randomUUID().toString(),
    val title: String,
    val amount: BigDecimal,
    val date: Date,
    val type: TransactionType,
    val description: String
)
