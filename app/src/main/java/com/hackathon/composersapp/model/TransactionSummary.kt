package com.hackathon.composersapp.model

import java.math.BigDecimal
import java.text.SimpleDateFormat
import java.util.*

data class TransactionSummary(
    val id: String = UUID.randomUUID().toString(),
    val title: String,
    val amount: BigDecimal,
    val date: Date,
    val type: TransactionType,
) {

    fun toTransaction() = Transaction(
        id = id,
        title = title,
        amount = amount,
        date = date,
        type = type,
        description = obtainRandomDescription()
    )

    private fun obtainRandomDescription() = listOf(
        "Sed ut perspiciatis unde omnis iste natus error.",
        "Nemo enim ipsam voluptatem quia voluptas sit.",
        "Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam.",
        "Quis autem vel eum iure reprehenderit qui in ea voluptate."
    )
        .shuffled()
        .first()
}

fun String.toDate(): Date = SimpleDateFormat("dd-MM-yyyy").parse(this)
