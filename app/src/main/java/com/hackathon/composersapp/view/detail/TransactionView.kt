package com.hackathon.composersapp.view.detail

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import com.hackathon.composersapp.R
import com.hackathon.composersapp.model.Transaction

@Composable
fun TransactionView(transaction: Transaction) = ConstraintLayout(
    modifier = Modifier
        .padding(16.dp)
        .fillMaxWidth()
        .semantics(mergeDescendants = true) {}
) {
    val (title, amount, type, description) = createRefs()

    Text(
        text = transaction.title,
        style = MaterialTheme.typography.h6,
        modifier = Modifier
            .constrainAs(title) {
                top.linkTo(parent.top)
                start.linkTo(parent.start)
            }
    )

    Text(
        text = stringResource(R.string.amount, transaction.amount.toString()),
        style = MaterialTheme.typography.h6,
        modifier = Modifier
            .constrainAs(amount) {
                baseline.linkTo(title.baseline)
                end.linkTo(parent.end)
            }
    )

    Text(
        text = transaction.description,
        style = MaterialTheme.typography.body2,
        modifier = Modifier
            .padding(top = 8.dp)
            .constrainAs(description) {
                top.linkTo(title.bottom)
                start.linkTo(parent.start)
            }
    )

    TransactionTypeView(
        type = transaction.type,
        modifier = Modifier
            .padding(top = 8.dp)
            .constrainAs(type) {
                top.linkTo(description.bottom)
                start.linkTo(parent.start)
            }
    )
}
