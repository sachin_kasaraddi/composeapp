package com.hackathon.composersapp.view.listing

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Payments
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.hackathon.composersapp.view.detail.TransactionDetailViewModel
import com.hackathon.composersapp.view.listing.TransactionListViewModel.State
import com.hackathon.composersapp.view.listing.TransactionListViewModel.State.Loading
import com.valentinilk.shimmer.shimmer

@ExperimentalFoundationApi
@Composable
@ExperimentalMaterialApi
fun TransactionListContentView(
    listState: LazyListState,
    listViewModel: TransactionListViewModel = hiltViewModel(),
    detailViewModel: TransactionDetailViewModel = hiltViewModel()
) {
    val state: State by listViewModel.state.collectAsState(initial = Loading)

    when (state) {
        is Loading -> {
            val list = mutableListOf<String>()
            repeat(15) {
                list.add("")
            }
            LazyColumn {
                stickyHeader {
                    Column(
                        modifier = Modifier
                            .fillMaxWidth()
                            .shimmer()
                    ) {
                        Text(
                            text = "",
                            style = MaterialTheme.typography.subtitle1,
                            modifier = Modifier
                                .fillMaxWidth()
                                .size(10.dp)
                                .background(Color.LightGray),
                            textAlign = TextAlign.Start,

                            )
                    }

                }
                items(items = list) {
                    Column(
                        modifier = Modifier.padding(horizontal = 16.dp, vertical = 8.dp)
                    ) {
                        Row(
                            verticalAlignment = Alignment.CenterVertically,
                            modifier = Modifier
                                .fillMaxWidth()
                                .shimmer()
                        ) {
                            Icon(
                                imageVector = Icons.Outlined.Payments,
                                contentDescription = null,
                                modifier = Modifier
                                    .size(50.dp)
                                    .padding(8.dp)
                                    .background(Color.LightGray)
                            )
                            Column(
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .padding(8.dp)
                                    .shimmer()
                            ) {
                                Text(
                                    text = "",
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .size(20.dp)
                                        .background(Color.LightGray),
                                    textAlign = TextAlign.Start
                                )
                                Text(
                                    text = "",
                                    textAlign = TextAlign.End,
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .size(20.dp)
                                        .background(Color.LightGray)
                                )
                            }
                        }
                    }
                }
            }
        }
        is State.Failed -> {

        }
        is State.Success -> {
            LazyColumn(state = listState) {
                (state as State.Success).transactions.forEach { date, list ->
                    stickyHeader {
                        Column(modifier = Modifier.fillMaxWidth()) {
                            Surface(color = Color.White, modifier = Modifier.fillParentMaxWidth()) {
                                Text(
                                    text = date.toLocaleString(),
                                    color = Color.Black,
                                    fontWeight = FontWeight.Bold,
                                    modifier = Modifier
                                        .padding(8.dp)
                                        .fillParentMaxWidth(),
                                    style = MaterialTheme.typography.subtitle2
                                )
                            }
                        }
                    }
                    items(
                        items = list
                    ) { transaction ->
                        TransactionListItem(
                            transaction = transaction,
                            onSelected = { id ->
                                detailViewModel.transactionId = id
                            }
                        )
                    }
                }

            }
        }
    }
}
