package com.hackathon.composersapp.view.detail

import androidx.annotation.StringRes
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Fastfood
import androidx.compose.material.icons.rounded.Healing
import androidx.compose.material.icons.rounded.LocalGroceryStore
import androidx.compose.material.icons.rounded.Payment
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.semantics.text
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.unit.dp
import com.hackathon.composersapp.R
import com.hackathon.composersapp.model.TransactionType
import com.hackathon.composersapp.model.TransactionType.*

@Composable
fun TransactionTypeView(
    type: TransactionType,
    modifier: Modifier = Modifier,
) = Row(
    modifier
        .background(
            color = MaterialTheme.colors.secondary,
            shape = CircleShape
        )
        .padding(8.dp)
) {
    val transactionType = stringResource(id = type.asStringRes())
    Icon(
        imageVector = type.asIconRes(),
        contentDescription = null
    )
    Text(
        text = stringResource(id = type.asStringRes()),
        style = MaterialTheme.typography.body2,
        modifier = Modifier
            .padding(start = 8.dp)
            .semantics {
                text =
                    AnnotatedString("Transaction type is $transactionType")
            }
    )
}

@StringRes
fun TransactionType.asStringRes() = when (this) {
    Bill -> R.string.bill
    Food -> R.string.food
    Grocery -> R.string.grocery
    Health -> R.string.health
}

fun TransactionType.asIconRes() = when (this) {
    Bill -> Icons.Rounded.Payment
    Food -> Icons.Rounded.Fastfood
    Grocery -> Icons.Rounded.LocalGroceryStore
    Health -> Icons.Rounded.Healing
}