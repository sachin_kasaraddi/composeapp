package com.hackathon.composersapp.view.listing

import androidx.lifecycle.ViewModel
import com.hackathon.composersapp.data.TransactionRepository
import com.hackathon.composersapp.model.Transaction
import com.hackathon.composersapp.model.TransactionSummary
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import java.util.*
import javax.inject.Inject

@HiltViewModel
class TransactionListViewModel @Inject constructor(private val repository: TransactionRepository) :
    ViewModel() {

    val state: Flow<State> = repository
        .obtainAll()
        .map<List<TransactionSummary>, State> { State.Success(transactions = it.groupBy { it.date }) }
        .catch { emit(State.Failed(cause = it)) }

    sealed class State {
        object Loading : State()
        data class Success(val transactions: Map<Date, List<TransactionSummary>>) : State()
        data class Failed(val cause: Throwable) : State()
    }
}
