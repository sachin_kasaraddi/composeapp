package com.hackathon.composersapp.view.detail

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.hackathon.composersapp.view.detail.TransactionDetailViewModel.State
import com.hackathon.composersapp.view.detail.TransactionDetailViewModel.State.*
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.semantics
import com.hackathon.composersapp.R
import kotlinx.coroutines.launch

@Composable
@ExperimentalMaterialApi
fun TransactionDetailView(
    bottomSheetState: BottomSheetScaffoldState,
    content: @Composable () -> Unit,
    viewModel: TransactionDetailViewModel = hiltViewModel()
) {
    val coroutineScope = rememberCoroutineScope()
    val state: State by viewModel
        .state
        .collectAsState(initial = Initial)

    BottomSheetScaffold(
        scaffoldState = bottomSheetState,
        sheetElevation = 16.dp,
        sheetContent = {
            coroutineScope.launch { bottomSheetState.bottomSheetState.expand() }

            when (state) {
                is Loading -> LoadingTransactionDetailView()
                is Failed -> ErrorTransactionDetailView()
                is Success -> TransactionView((state as Success).transaction)
            }
        },
        sheetPeekHeight = 1.dp,
        content = { content() }
    )
}

@Composable
fun LoadingTransactionDetailView() {
    Box(
        contentAlignment = Alignment.Center,
        modifier = Modifier
            .fillMaxWidth()
            .padding(16.dp)
    ) {
        CircularProgressIndicator()
    }
}

@Composable
fun ErrorTransactionDetailView() {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(16.dp)
            .semantics(mergeDescendants = true){}
    ) {
        Text(
            text = stringResource(id = R.string.error_title),
            style = MaterialTheme.typography.h6,
            color = MaterialTheme.colors.error
        )

        Text(
            text = stringResource(R.string.error_message),
            style = MaterialTheme.typography.body2,
            color = MaterialTheme.colors.error,
            modifier = Modifier.padding(top = 8.dp)
        )
    }
}
