package com.hackathon.composersapp.view.detail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.hackathon.composersapp.data.TransactionRepository
import com.hackathon.composersapp.model.Transaction
import com.hackathon.composersapp.view.detail.TransactionDetailViewModel.State.*
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class TransactionDetailViewModel @Inject constructor(private val repository: TransactionRepository) :
    ViewModel() {

    private val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
        _state.value = Failed(cause = throwable)
    }

    private val _state = MutableStateFlow<State>(value = Initial)
    val state: StateFlow<State> get() = _state

    var transactionId: String = ""
        set(value) {
            field = value
            obtainTransaction()
        }

    private fun obtainTransaction() = viewModelScope.launch(context = IO + exceptionHandler) {
        _state.value = Loading
        _state.value = Success(repository.obtainById(transactionId = transactionId))
    }

    sealed class State {
        object Initial : State()
        object Loading : State()
        data class Success(val transaction: Transaction) : State()
        class Failed(val cause: Throwable) : State()
    }
}
