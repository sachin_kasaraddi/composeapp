package com.hackathon.composersapp.view.listing

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.Divider
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import com.hackathon.composersapp.R
import com.hackathon.composersapp.model.TransactionSummary
import com.hackathon.composersapp.view.detail.asIconRes
import com.hackathon.composersapp.view.detail.asStringRes

@Composable
fun TransactionListItem(transaction: TransactionSummary, onSelected: (String) -> Unit) {
    val detailLabel =
        stringResource(R.string.detaillabel, transaction.title)
    Column(modifier = Modifier.clickable(onClickLabel = detailLabel) { onSelected(transaction.id) }) {
        ConstraintLayout(
            modifier = Modifier
                .padding(horizontal = 16.dp, vertical = 8.dp)
                .fillMaxWidth()
        ) {
            val (icon, title, amount) = createRefs()

            Icon(
                imageVector = transaction.type.asIconRes(),
                contentDescription = stringResource(id = transaction.type.asStringRes()),
                modifier = Modifier
                    .size(50.dp)
                    .padding(8.dp)
                    .constrainAs(icon) {
                        top.linkTo(parent.top)
                        start.linkTo(parent.start)
                    }
            )

            Text(
                text = transaction.title,
                style = MaterialTheme.typography.subtitle1,
                textAlign = TextAlign.Start,
                modifier = Modifier
                    .fillMaxWidth()
                    .constrainAs(title) {
                        top.linkTo(parent.top)
                        bottom.linkTo(parent.bottom)
                        start.linkTo(icon.end)
                    }
            )

            Text(
                text = stringResource(id = R.string.amount, transaction.amount.toString()),
                fontWeight = FontWeight.Bold,
                style = MaterialTheme.typography.subtitle2,
                textAlign = TextAlign.End,
                modifier = Modifier
                    .fillMaxWidth()
                    .constrainAs(amount) {
                        top.linkTo(parent.top)
                        bottom.linkTo(parent.bottom)
                        end.linkTo(parent.end)
                    }
            )
        }
        Divider(
            color = Color.LightGray,
            thickness = 0.2.dp,
            modifier = Modifier.padding(4.dp)
        )
    }
}