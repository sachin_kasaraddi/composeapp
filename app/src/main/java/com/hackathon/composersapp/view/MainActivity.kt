package com.hackathon.composersapp.view

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material.*
import androidx.compose.material.BottomSheetValue.Collapsed
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.semantics.text
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.tooling.preview.Preview
import com.hackathon.composersapp.R
import com.hackathon.composersapp.view.detail.TransactionDetailView
import com.hackathon.composersapp.view.listing.TransactionListContentView
import com.hackathon.composersapp.view.theme.ComposersAppTheme
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
@ExperimentalFoundationApi
@ExperimentalMaterialApi
class MainActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ComposersAppTheme {
                ComposeApp()
            }
        }
    }

    @ExperimentalFoundationApi
    @ExperimentalMaterialApi
    @Composable
    fun ComposeApp() = Scaffold(
        topBar = {
            TopAppBar(title = {
                Text(getString(R.string.app_name), modifier = Modifier.semantics {
                    text =
                        AnnotatedString("Fake bank")
                })
            })
        },
        content = {
            val bottomSheetState =
                rememberBottomSheetScaffoldState(bottomSheetState = BottomSheetState(Collapsed))
            val listState = rememberLazyListState()
            val coroutineScope = rememberCoroutineScope()

            if (listState.isScrollInProgress) coroutineScope.launch { bottomSheetState.bottomSheetState.collapse() }

            TransactionDetailView(
                bottomSheetState = bottomSheetState,
                content = { TransactionListContentView(listState) }
            )
        }
    )

    @ExperimentalFoundationApi
    @Preview("Light Theme", widthDp = 360, heightDp = 640)
    @Composable
    fun LightPreview() =
        ComposersAppTheme { ComposeApp() }

    @ExperimentalFoundationApi
    @Preview("Dark Theme", widthDp = 360, heightDp = 640)
    @Composable
    fun DarkPreview() =
        ComposersAppTheme(darkTheme = true) { ComposeApp() }
}