package com.hackathon.composersapp.data.local

import com.hackathon.composersapp.model.Transaction
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Local fake [Transaction] detail provider.
 *
 * It holds a list with all the available transactions, in case that no transaction match
 * with the requested id, the default `block` is executed in order to create one, which is added
 * to the list.
 */
@Singleton
class TransactionDetailProvider @Inject constructor() {

    private val transactions = mutableListOf<Transaction>()

    fun obtainBy(id: String, block: () -> Transaction): Transaction {
        var detail = transactions.firstOrNull { it.id == id }

        if (detail == null) {
            detail = block()
            transactions.add(detail)
        }

        return detail
    }
}
