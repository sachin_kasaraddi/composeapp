package com.hackathon.composersapp.data

import com.hackathon.composersapp.data.local.TransactionDetailProvider
import com.hackathon.composersapp.data.local.TransactionSummaryDataSource
import com.hackathon.composersapp.model.Transaction
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.random.Random

@Singleton
class TransactionRepository @Inject constructor(
    private val transactionSummarySource: TransactionSummaryDataSource,
    private val transactionSource: TransactionDetailProvider,
) {

    fun obtainAll() = flow {
        delay(timeMillis = SEARCH_DELAY_IN_MILLS)

        emit(transactionSummarySource.all)
    }

    suspend fun obtainById(transactionId: String): Transaction = withContext(Dispatchers.IO) {
        delay(timeMillis = DETAIL_DELAY_IN_MILLS)
        simulateRandomError()

        transactionSource.obtainBy(id = transactionId) {
            transactionSummarySource.all.first { it.id == transactionId }.toTransaction()
        }
    }

    private fun simulateRandomError() {
        if (Random.nextInt(1,  4) == 3) throw IllegalArgumentException()
    }

    companion object {
        private const val SEARCH_DELAY_IN_MILLS = 2000L
        private const val DETAIL_DELAY_IN_MILLS = 1000L
    }
}
