package com.hackathon.composersapp.data.local

import com.hackathon.composersapp.model.TransactionSummary
import com.hackathon.composersapp.model.TransactionType
import com.hackathon.composersapp.model.toDate
import java.math.BigDecimal
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Local fake transaction summary provider.
 * It provides a hardcoded list of [TransactionSummary].
 */
@Singleton
class TransactionSummaryDataSource @Inject constructor() {

    val all: List<TransactionSummary> = listOf(
        TransactionSummary(
            title = "Dirk Broek",
            date = "21-10-2021".toDate(),
            amount = BigDecimal.valueOf(12.3),
            type = TransactionType.Grocery
        ),
        TransactionSummary(
            title = "Transavia",
            date = "21-10-2021".toDate(),
            amount = BigDecimal.valueOf(10),
            type = TransactionType.Bill
        ),
        TransactionSummary(
            title = "Copy Plaza",
            date = "20-10-2021".toDate(),
            amount = BigDecimal.valueOf(9.3),
            type = TransactionType.Health
        ),
        TransactionSummary(
            title = "Adyen",
            date = "20-10-2021".toDate(),
            amount = BigDecimal.valueOf(12.3),
            type = TransactionType.Food
        ),
        TransactionSummary(
            title = "Dirk Broek",
            date = "18-10-2021".toDate(),
            amount = BigDecimal.valueOf(50.8),
            type = TransactionType.Grocery
        ),
        TransactionSummary(
            title = "Youfone",
            date = "17-10-2021".toDate(),
            amount = BigDecimal.valueOf(26.7),
            type = TransactionType.Bill
        ),
        TransactionSummary(
            title = "TooGoodToGo",
            date = "17-10-2021".toDate(),
            amount = BigDecimal.valueOf(40),
            type = TransactionType.Grocery
        ),
        TransactionSummary(
            title = "Bibliotheek",
            date = "14-10-2021".toDate(),
            amount = BigDecimal.valueOf(3.3),
            type = TransactionType.Grocery
        ),
        TransactionSummary(
            title = "Usai Valentina",
            date = "12-10-2021".toDate(),
            amount = BigDecimal.valueOf(76),
            type = TransactionType.Health
        ),
        TransactionSummary(
            title = "ABN AMRO Bank",
            date = "12-10-2021".toDate(),
            amount = BigDecimal.valueOf(100),
            type = TransactionType.Bill
        ),
        TransactionSummary(
            title = "Youfone",
            date = "11-10-2021".toDate(),
            amount = BigDecimal.valueOf(45.8),
            type = TransactionType.Bill
        ),
        TransactionSummary(
            title = "Oleander",
            date = "08-10-2021".toDate(),
            amount = BigDecimal.valueOf(43.2),
            type = TransactionType.Health
        ),
        TransactionSummary(
            title = "ABN AMRO Bank",
            date = "07-10-2021".toDate(),
            amount = BigDecimal.valueOf(95.2),
            type = TransactionType.Bill
        ),
        TransactionSummary(
            title = "Youfone",
            date = "07-10-2021".toDate(),
            amount = BigDecimal.valueOf(12.65),
            type = TransactionType.Bill
        ),
        TransactionSummary(
            title = "Carl Sagan",
            date = "07-10-2021".toDate(),
            amount = BigDecimal.valueOf(226.6),
            type = TransactionType.Food
        ),
        TransactionSummary(
            title = "Albert Heijn",
            date = "05-10-2021".toDate(),
            amount = BigDecimal.valueOf(45),
            type = TransactionType.Grocery
        ),
        TransactionSummary(
            title = "Oleander",
            date = "04-10-2021".toDate(),
            amount = BigDecimal.valueOf(24.5),
            type = TransactionType.Health
        ),
        TransactionSummary(
            title = "Dirk Broek",
            date = "04-10-2021".toDate(),
            amount = BigDecimal.valueOf(13.1),
            type = TransactionType.Grocery
        ),
        TransactionSummary(
            title = "Bunq",
            date = "01-10-2021".toDate(),
            amount = BigDecimal.valueOf(11.4),
            type = TransactionType.Bill
        ),
        TransactionSummary(
            title = "Oleander",
            date = "20-09-2021".toDate(),
            amount = BigDecimal.valueOf(31.2),
            type = TransactionType.Food
        ),
        TransactionSummary(
            title = "Transavia",
            date = "18-09-2021".toDate(),
            amount = BigDecimal.valueOf(12.4),
            type = TransactionType.Food
        ),
        TransactionSummary(
            title = "Samsung",
            date = "10-09-2021".toDate(),
            amount = BigDecimal.valueOf(102),
            type = TransactionType.Bill
        ),
        TransactionSummary(
            title = "Apple Store",
            date = "05-09-2021".toDate(),
            amount = BigDecimal.valueOf(43.87),
            type = TransactionType.Bill
        ),
    )
}